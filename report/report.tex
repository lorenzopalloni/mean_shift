\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{float}

%\usepackage{hyperref}

\usepackage{url}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{booktabs}
\usepackage{adjustbox}
\usepackage{boldline,multirow}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{subcaption}
\usepackage{cleveref}
\usepackage{mathtools}
\usepackage{graphics}
\usepackage[font=small,skip=0.5pt]{caption}

\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}
\DeclarePairedDelimiter\abs{\lvert}{\rvert}%

\newtheorem{definition}{Definition}

\algnewcommand\algorithmicforeach{\textbf{for each}}
\algdef{S}[FOR]{ForEach}[1]{\algorithmicforeach\ #1\ \algorithmicdo}
\algdef{SE}[DOWHILE]{Do}{doWhile}{\algorithmicdo}[1]{\algorithmicwhile\ #1}%

\cvprfinalcopy % *** Uncomment this line for the final submission

\def\cvprPaperID{****} % *** Enter the CVPR Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ifcvprfinal\pagestyle{empty}\fi
\begin{document}

%%%%%%%%% TITLE
\title{PC-2018/19 Mean Shift Clustering in C++/OpenMP}

\author{
    Lorenzo Palloni\\
    {\tt\small lorenzo.palloni@stud.unifi.it}
}

\maketitle
\thispagestyle{empty}
%-------------------------------------------------------------------------

\begin{abstract}
In this report we compare average runtimes of the executions of the mean shift clustering between a sequential and two parallel implementations.

Firstly we define the math behind the mean shift clustering defining kernels, the mean shift and the mean shift procedure. Afterwards we describe sequential and parallel implementations of mean shift clustering. We end showing the results of the average runtimes and the average speedups comparing sequential execution with parallel executions of mean shift clustering applied to datasets of different sizes.

\end{abstract}
%-------------------------------------------------------------------------

\section{Introduction}
Mean shift is an iterative procedure that shifts each data point to the average of the data points in its neighborhood~\cite{cheng1995}. Mean shift is an embarrassingly parallel problem, since that the shifting phase can be divided into multiple tasks (one for each data point) and each task have no need to communicate with each other.

To parallelize the mean shift procedure we use OpenMP, that is an implicit threading frameworks that allows programmers to greatly simplify the implementation of concurrent programs at the expense of expressiveness and flexibility. More formally is a set of compiler directives, library routines and environment variables that can be used to specify high-level parallelism in Fortran and C/C++ programs.

In the next section we define \textit{kernels} and then the \textit{mean shift}. In~\cref{sec:sec12} we define the \textit{mean shift procedure}. More forward in~\cref{sec:sec2} we describe a sequential and two parallel implementations of the \textit{mean shift clustering}. In the end we compare runtimes of all the implementations described.

\vspace{3cm}
%------------------------------------------------------------------------------
\begin{figure*}[htbp]
\begin{center}
\begin{subfigure}{0.45\linewidth}
    \includegraphics[width=\linewidth]{./figures/generated_3000.png}
    \caption{Generated values.}
\end{subfigure}
~
\begin{subfigure}{0.45\linewidth}
    \includegraphics[width=\linewidth]{./figures/clusterized_3000.png}
    \caption{Predicted values.}
\end{subfigure}
\end{center}
\captionsetup{skip=0pt}
\caption{Visualization of generated values (left) and their classification using mean shift clustering (right).}\label{fig:fig1}
\end{figure*}
%-------------------------------------------------------------------------

\subsection{Kernels}

\begin{definition}[Kernels]\label{def:def1}
Let $\mathbf{x} \in \mathbb{R}^p$ be a $p$-dimensional data point (vector) of real numbers. Denoting with $\norm{\mathbf{x}}$ the norm of $\mathbf{x}$, i.e. a non-negative number such that $\norm{\mathbf{x}}^2 = \sum_{j=1}^{p} \abs{x_j}^2$. A function $K:\mathbb{R}^p \rightarrow \mathbb{R}$ is said to be a kernel if there exists a function $k:[0, \infty] \rightarrow \mathbb{R}$, such that
\begin{align*}
K(\mathbf{x}) = k(\norm{\mathbf{x}}^2)
\end{align*}
and
\begin{itemize}[nosep]
\item $k$ is non-negative;
\item $k$ is non-increasing $k(a) \geq k(b)$, if $a < b$;
\item $k$ is piecewise continuous and $\int_{0}^{\infty} k(r)dr < \infty$.
\end{itemize}
\end{definition}

In the implementations of this report we use a \textit{Gaussian kernel}, defined by
\begin{align*}
K(\mathbf{x}) = \exp (- \norm{\mathbf{x}}^2 / 2\sigma^2),
\end{align*}
with $\sigma \in \mathbb{R}$.

\subsection{Mean shift procedure}\label{sec:sec12}
\begin{definition}[Mean shift]\label{def:def2}
Let $X \in \mathbb{R}^{n \times p}$ be a dataset (matrix) of dimensions $n \times p$. Denoting $x_{ij}$ as the value of the $j$-th dimension of the $i$-th data point $\mathbf{x}_i \in \mathbb{R}^p$. We define the \textit{mean shift} of the $i'$-th data point as, with $i, i' \in \{1, \dots, n\}$ and $j = \{1, \dots, p\}$.
\begin{align*}
m(\mathbf{x}_{i'}) &= \frac{\sum_{i=1}^{n}K(\mathbf{x}_{i} - \mathbf{x}_{i'})\cdot\mathbf{x}_{i}}{\sum_{i=1}^{n}K(\mathbf{x}_{i} - \mathbf{x}_{i'})} = \\
&= \frac{\sum_{i=1}^{n}K \left( \begin{bmatrix} x_{i1} - x_{i'1}\\ \vdots \\ x_{ij} - x_{i'j}\\ \vdots \\ x_{ip} - x_{i'p}\end{bmatrix} \right) \cdot \mathbf{x}_{i}} {\sum_{i=1}^{n}K \left( \begin{bmatrix} x_{i1} - x_{i'1}\\ \vdots \\ x_{ij} - x_{i'j}\\ \vdots \\ x_{ip} - x_{i'p}\end{bmatrix} \right) }
\end{align*}
where $K(\cdot)$ is a kernel as defined in~\cref{def:def1}.
\end{definition}

The \textit{mean shift procedure} applies for each data point $\mathbf{x}_i$ in the dataset the mean shift $m(\cdot)$ iteratively.

%-------------------------------------------------------------------------
\section{Mean shift clustering}\label{sec:sec2}
A sequential implementation of the mean shift procedure is showed in~\cref{alg:alg1}, keeping the same indices convention used in~\cref{def:def1,def:def2}. The algorithm performs for each data point a mean shift until convergence. Each data point is said to be converged if it remains unchanged after an application of mean shift. Mean shift is showed in~\cref{alg:alg2} and it is pretty self-explained based on~\cref{def:def1,def:def2}.

\begin{algorithm}[H]
	\caption{meanShiftProcedure}
    \label{alg:alg1}
    \hspace*{\algorithmicindent} \textbf{Input}: dataset $X$ \\
    \hspace*{\algorithmicindent} \textbf{Output}: shifted dataset $S$
	\begin{algorithmic}[1]
        \State $S \gets X$
        \For{$i = 1$ to $n$}
        \Do
        \State $temp \gets S[i]$
        \State $S[i] \gets meanShift(S[i], X)$
        \doWhile{$S[i] \neq temp$}
        \EndFor
        \State \Return{$S$}
	\end{algorithmic}
\end{algorithm}
\begin{algorithm}[H]
	\caption{meanShift}
    \label{alg:alg2}
    \hspace*{\algorithmicindent} \textbf{Input}: data point $\mathbf{x}$, dataset $X$ \\
    \hspace*{\algorithmicindent} \textbf{Output}: shifted data point $\mathbf{s}$
	\begin{algorithmic}[1]
        \State $s \gets 0$
        \For{$i = 1$ to $n$}
        \State $s \gets s + \frac{K(X[i] - \mathbf{x})X[i]}{K(X[i] - \mathbf{x})}$
        \EndFor
        \State \Return{$s$}
	\end{algorithmic}
\end{algorithm}

\textit{Mean shift clustering} takes as input the shifted dataset build with~\cref{alg:alg1} and maps equal shifted data points to the same cluster. Thus we will have as many clusters as the number of unique shifted data points.

An example of an application of the mean shift clustering is showed in~\cref{fig:fig1} where 3000 data points are right clusterized by the algorithm.

\subsection{Parallel mean shift clustering}
The parallel version of the mean shift clustering using OpenMP framework is showed in~\cref{alg:alg3}. With only one line of code (line 2), OpenMP directive generates code at compile time that allows to parallelize the outer for loop. All the threads involved execute the for loop concurrently. By default OpenMP directive leaves the compiler to choose the scheduling type among various. In this report we have been implemented and compared two type of scheduling:
\begin{itemize}[nosep]
\item[$\bullet$]in \texttt{static} the loop is divided into equal-sized chunks or as equal as possible where the number of loop iterations is not evenly divisible by the number of threads multiplied by the chunk size.
\item[$\bullet$]in \texttt{dynamic} a work queue give a chunk-sized block of loop iterations to each thread. Ones a thread has finished, it ask for another block of loop iterations chunk-sized. By default the chunk size per block of loop iterations is set to 1.
\end{itemize}
\begin{algorithm}[H]
	\caption{parallelMeanShiftProcedure}
    \label{alg:alg3}
    \hspace*{\algorithmicindent} \textbf{Input}: dataset $X$ \\
    \hspace*{\algorithmicindent} \textbf{Output}: shifted dataset $S$
	\begin{algorithmic}[1]
        \State $S \gets X$
        \State \#pragma omp parallel for
        \For{$i = 1$ to $n$}
        \Do
        \State $temp \gets S[i]$
        \State $S[i] \gets meanShift(S[i], X)$
        \doWhile{$S[i] \neq temp$}
        \EndFor
        \State \Return{$S$}
	\end{algorithmic}
\end{algorithm}
In the next section we compare runtimes of the serial and parallel implementations of mean shift clustering.
%------------------------------------------------------------------------------
%------------------------------------------------------------------------------
\section{Results}\label{sec:sec3}
All the experiments are executed on a dataset build starting from four 3-dimensional data points, generated from different uniform distributions. We added Gaussian noise to these data points in order to extract four datasets of dimensions $n \times 3$ with $n \in \{100, 1000, 3000, 10000\}$.
Sequential and parallel implementations of mean shift clustering are executed with the following fixed parameters:
\begin{itemize}[nosep]
\item[$\bullet$] $\sigma$ paramter of~\cref{def:def1} set to the value 2, that it was chosen based on the standard deviations of the Gaussian distribution where the noise to augment the data comes from;
\item[$\bullet$] \textit{maximum number of iterations} set to the value 100, that is an upper bound of the times that a data point can be shifted, if it reaches or not the convergence.
\end{itemize}

We compare sequential and parallel executions in function of the OpenMP loop scheduling type: \textit{static} and \textit{dynamic} scheduling.
All the experiments are executed by 10 times in order to compute averages and relative standard deviations, on a CPU i7-8750H with 6 cores (12 threads using hyper-threading). All the code are available online\footnote{https://gitlab.com/deeplego/mean\_shift}.

Average runtimes (in milliseconds [ms]) and average speedup (marked with an X as suffix) between sequential execution and parallel with static scheduling execution are showed in~\cref{tab:tab1}. The results relative to the comparison with parallel dynamic scheduling are showed in~\cref{tab:tab2}.

%------------------------------------------------------------------------------

%------------------------------------------------------------------------------
\begin{table}[htbp]
\begin{adjustbox}{width=\linewidth,center}
\begin{tabular}{llll}
\cline{2-4}
Sizes & Sequential & Parallel & Speedup \\
\clineB{1-4}{1.5}
100    & $   7.04  \pm 0.11     $ [ms]  & $  5.14 \pm 1.73 $ [ms] & $  1.55X \pm 0.64 $ \\
\hline
1000   & $  1489.34 \pm 1.60    $ [ms]  & $  368.89 \pm 5.60 $ [ms] & $  \textbf{4.04X} \pm 0.06 $ \\
\hline
3000   & $ 9093.52 \pm 29.28  $ [ms]  & $  2357.39 \pm 32.63 $ [ms] & $  3.86X \pm 0.04 $ \\
\hline
10000  & $129219.70 \pm 64.21 $ [ms]  & $ 36960.43 \pm 718.77 $ [ms] & $ 3.50X \pm 0.07 $ \\
\bottomrule
\end{tabular}
\end{adjustbox}
\caption{Runtimes of sequential vs parallel executions (static scheduling).}\label{tab:tab1}
\end{table}
%------------------------------------------------------------------------------
%------------------------------------------------------------------------------
\begin{table}[htbp]
\begin{adjustbox}{width=\linewidth,center}
\begin{tabular}{llll}
\cline{2-4}
Sizes & Sequential & Parallel & Speedup \\
\clineB{1-4}{1.5}
100    & $    7.14 \pm 0.11     $ [ms]  & $   3.56 \pm 1.48 $ [ms] & $  2.39X \pm 1.02 $ \\
\hline
1000   & $  1522.40 \pm 2.54    $ [ms]  & $  212.33 \pm 4.56 $ [ms] & $  \textbf{7.17X} \pm 0.15 $ \\
\hline
3000   & $  9743.32 \pm 232.27  $ [ms]  & $ 1515.54 \pm 66.31 $ [ms] & $  6.44X \pm 0.21 $ \\
\hline
10000  & $ 130285.30 \pm 267.16 $ [ms]  & $ 19971.52 \pm 204.98 $ [ms] & $ 6.52X \pm 0.07 $ \\
\bottomrule
\end{tabular}
\end{adjustbox}
\caption{Runtimes of sequential vs parallel executions (dynamic scheduling with 1 thread per block).}\label{tab:tab2}
\end{table}
%------------------------------------------------------------------------------
%------------------------------------------------------------------------------
%------------------------------------------------------------------------------
The informations of~\cref{tab:tab1,tab:tab2} are summarized in~\cref{fig:fig2}. We can see that the trend of the average speedup in function of the dataset size using dynamic scheduling is greater than the same trend using static scheduling. The maximum average speedup reached in both cases with 1000 data points is $4.14$X for static scheduling e $7.17$X with dynamic scheduling.

In~\cref{fig:fig3} we can figure out an increasing, \textit{linear trend} of the average speedup in function of the number of cores, typical of the embarrassingly parallelizable problem. To achieve this plot we deactivate hyper-threading.
%------------------------------------------------------------------------------
%------------------------------------------------------------------------------
%------------------------------------------------------------------------------
\begin{figure}[H]
\begin{center}
\includegraphics[width=\linewidth,keepaspectratio]{./figures/small_speedups.png}
\end{center}
\caption{Trends of average speedups in function of dataset sizes and scheduling types.}\label{fig:fig2}
\captionsetup{skip=0pt}
\end{figure}
%------------------------------------------------------------------------------
\begin{figure}[H]
\begin{center}
\includegraphics[width=\linewidth]{./figures/small_speedups_vs_nCores.png}
\end{center}
\caption{Trend of average speedup in function of the number of cores.}\label{fig:fig3}
\end{figure}
%------------------------------------------------------------------------------
%------------------------------------------------------------------------------
\section{Conclusions}
We implemented a sequential mean shift clustering, then with the OpenMP framework we parallelize it with only one line of code implementing two different type of thread scheduling.

Exploiting the trade-off between quantity of workload and the awaiting time until the last thread finish its execution we achieve an average speedup of $7.17$X with a dynamic scheduling with one loop iteration per block in the internal work queue of the scheduling.

%------------------------------------------------------------------------------
\clearpage
{\small
\bibliographystyle{ieee}
\begin{thebibliography}{9}

\bibitem{cheng1995}{Cheng, Yizong. "Mean shift, mode seeking, and clustering." IEEE transactions on pattern analysis and machine intelligence 17.8 (1995): 790-799.}

\end{thebibliography}
}
%------------------------------------------------------------------------------
%------------------------------------------------------------------------------
%\begin{figure*}[htbp]
%\begin{center}
%\begin{subfigure}{0.50\linewidth}
%    \includegraphics[width=0.9\linewidth,keepaspectratio]{./figures/small_speedups.png}
%    \caption{Trends of average speedups in function of dataset sizes and scheduling types.}
%\end{subfigure}
%\begin{subfigure}{0.50\linewidth}
%    \includegraphics[width=\linewidth]{./figures/small_speedups_vs_nCores.png}
%    \caption{Predicted values.}
%\end{subfigure}
%\end{center}
%%\captionsetup{skip=0pt}
%\caption{Trend of average speedup in function of the number of cores.}\label{fig:fig1}
%\end{figure*}
%-------------------------------------------------------------------------
%------------------------------------------------------------------------------
%\begin{figure*}[htpb!]
%\begin{center}
%\includegraphics[width=0.9\linewidth,keepaspectratio]{./figures/speedups.png}
%\end{center}
%\caption{Trends of average speedups in function of dataset sizes and scheduling types.}\label{fig:fig2}
%\end{figure*}
%%------------------------------------------------------------------------------
%\begin{figure*}[htpb!]
%\begin{center}
%\includegraphics[width=0.9\linewidth,keepaspectratio]{./figures/speedups_vs_nCores.png}
%\end{center}
%\caption{Trend of average speedup in function of the number of cores.}\label{fig:fig3}
%\end{figure*}
%------------------------------------------------------------------------------

\end{document}
