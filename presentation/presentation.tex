\documentclass{beamer}

\mode<presentation> {
\usetheme{Madrid}
}
\beamertemplatenavigationsymbolsempty

\usepackage{pacman} % Allows you to be the best presentator ever :D
\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage[utf8]{inputenc}
\usepackage{float}
\usepackage{subcaption}

\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{mathtools}

\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}
\DeclarePairedDelimiter\abs{\lvert}{\rvert}%

\algnewcommand\algorithmicforeach{\textbf{for each}}
\algdef{S}[FOR]{ForEach}[1]{\algorithmicforeach\ #1\ \algorithmicdo}
\algdef{SE}[DOWHILE]{Do}{doWhile}{\algorithmicdo}[1]{\algorithmicwhile\ #1}%

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\title[PC-2018/19 Mean Shift Clustering]{PC-2018/19 Mean Shift Clustering in C++/OpenMP}
\subtitle{Parallel Computing Course}
\author{Lorenzo Palloni}
\institute[]{
    Università Degli Studi di Firenze \\
    \medskip
    \textit{lorenzo.palloni@stud.unifi.it }
}
\date{\today}

\begin{document}

\begin{frame}
\titlepage % Print the title page as the first slide
\end{frame}

%----------------------------------------------------------------------------------------
%	PRESENTATION SLIDES
%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------
%   TABLE OF CONTENTES	
%----------------------------------------------------------------------------------------
\begin{frame}
\tableofcontents
\end{frame}
%----------------------------------------------------------------------------------------
\section{Introduction}
\begin{frame}
\frametitle{Introduction}
\begin{itemize}
\item In this project we compare average runtimes of the executions of the \textbf{mean shift clustering} between
    \begin{itemize}
    \item a \textbf{sequential} and
    \item two \textbf{parallel} implementations
    \end{itemize}
\item The implementations are done using \textbf{C++} programming language and the \textbf{OpenMP} framework.
\item In the next slides we introduce some \textbf{basic concepts} of mean shift clustering and OpenMP in order to better understand the results.
\end{itemize}
\end{frame}
%----------------------------------------------------------------------------------------
\subsection{Kernels}
\begin{frame}
\begin{definition}[Kernels]
Let
\begin{itemize}
\item $\mathbf{x} \in \mathbb{R}^p$ be a $p$-dimensional \textbf{data point} (vector) of real numbers;
\item $\norm{\mathbf{x}}$ is the \textbf{norm} of $\mathbf{x}$.
\end{itemize}
A function $K:\mathbb{R}^p \rightarrow \mathbb{R}$ is said to be a \textbf{kernel} if there exists a function $k:[0, \infty] \rightarrow \mathbb{R}$, such that
\begin{align*}
K(\mathbf{x}) = k(\norm{\mathbf{x}}^2)
\end{align*}
and $k$ is:
\begin{itemize}
\item non-negative;
\item non-increasing;
\item piecewise continuous;
\item such that $\int_{0}^{\infty} k(r)dr < \infty$.
\end{itemize}
\end{definition}
\end{frame}
%----------------------------------------------------------------------------------------
\subsection{Mean shift}
\begin{frame}
\begin{definition}[Mean shift]
Let
\begin{itemize}
\item $X \in \mathbb{R}^{n \times p}$ is a \textbf{dataset} (matrix) of dimensions $n \times p$.
\item $i, i^* \in \{1, \dots, n\}$ and $j = \{1, \dots, p\}$.
\end{itemize}
We define the \textbf{mean shift} of the $i^*$-th data point $\mathbf{x}_{i^*}$ as
\begin{align*}
m(\mathbf{x}_{i^*}) &= \frac{\sum_{i=1}^{n}K(\mathbf{x}_{i} - \mathbf{x}_{i^*})\cdot\mathbf{x}_{i}}{\sum_{i=1}^{n}K(\mathbf{x}_{i} - \mathbf{x}_{i^*})}
\end{align*}
where
\begin{itemize}
\item $K(\cdot)$ is a \textbf{kernel}.
\end{itemize}
\end{definition}
\end{frame}
%----------------------------------------------------------------------------------------
\subsection{Mean shift procedure}
\begin{frame}
\frametitle{Mean shift procedure}
\begin{itemize}
\item In our implementations we use a \textbf{Gaussian kernel}, defined by
\begin{align*}
K(\mathbf{x}) = \exp (- \norm{\mathbf{x}}^2 / 2\sigma^2),
\end{align*}
with $\sigma \in \mathbb{R}$.
\item We define \textbf{mean shift procedure} as the iteratively application of the mean shift $m(\cdot)$ to each data point $\mathbf{x}_i$ in the dataset.
\end{itemize}
\end{frame}
%----------------------------------------------------------------------------------------
\subsection{Mean Shift Clustering}
\begin{frame}
\frametitle{Mean Shift Clustering}
\begin{itemize}
\item is an algorithm that performs the mean shift procedure until each data point has reached its \textbf{convergence}.
\item A data point is said to be \textbf{converged} if it remains unchanged after an application of mean shift.
\item An example of application of mean shift clustering to 3000 data points:
\end{itemize}

\begin{center}
\begin{figure}
\end{figure}
\includegraphics[width=\textwidth,height=0.8\textheight,keepaspectratio]{figures/clusterized_3000.png}
\end{center}

\end{frame}
%----------------------------------------------------------------------------------------
\section{Implementations}
\subsection{Sequential implementation}
\begin{frame}
\frametitle{Sequential implementation (1/3)}

\begin{algorithm}[H]
	\caption{meanShiftProcedure}
    \label{alg:alg1}
    \hspace*{\algorithmicindent} \textbf{Input}: dataset $X$ \\
    \hspace*{\algorithmicindent} \textbf{Output}: shifted dataset $S$
	\begin{algorithmic}[1]
        \State $S \gets X$
        \For{$i = 1$ to $n$}
        \Do
        \State $temp \gets S[i]$
        \State $S[i] \gets meanShift(S[i], X)$ $\ \ \ \ \ \ \leftarrow$ 
        \doWhile{$S[i] \neq temp$}
        \EndFor
        \State \Return{$S$}
	\end{algorithmic}
\end{algorithm}

\end{frame}
%----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Sequential implementation (2/3)}

\begin{algorithm}[H]
	\caption{meanShift}
    \label{alg:alg2}
    \hspace*{\algorithmicindent} \textbf{Input}: data point $\mathbf{x}$, dataset $X$ \\
    \hspace*{\algorithmicindent} \textbf{Output}: shifted data point $\mathbf{s}$
	\begin{algorithmic}[1]
        \State $s \gets 0$
        \For{$i = 1$ to $n$}
        \State $s \gets s + \frac{K(X[i] - \mathbf{x})X[i]}{K(X[i] - \mathbf{x})}$ $\ \ \ \ \ \ \leftarrow$ 
        \EndFor
        \State \Return{$s$}
	\end{algorithmic}
\end{algorithm}
\end{frame}
%----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Sequential implementation (3/3)}
\textbf{Mean shift clustering}
\begin{enumerate}
\item takes as input the \textbf{shifted dataset} build with \textbf{Algorithm 1}.
\item maps equal shifted data points to the same \textbf{cluster}.
\end{enumerate}
\begin{itemize}
\item Thus we will have as many clusters as the number of unique shifted data points
\end{itemize}
\end{frame}
%----------------------------------------------------------------------------------------
\subsection{Parallel implementations}
\begin{frame}
\frametitle{Parallel implementations (1/2)}

\begin{algorithm}[H]
	\caption{parallelMeanShiftProcedure}
    \label{alg:alg3}
    \hspace*{\algorithmicindent} \textbf{Input}: dataset $X$ \\
    \hspace*{\algorithmicindent} \textbf{Output}: shifted dataset $S$
	\begin{algorithmic}[1]
        \State $S \gets X$
        \State \#pragma omp parallel for $\ \ \ \ \ \ \leftarrow$ 
        \For{$i = 1$ to $n$}
        \Do
        \State $temp \gets S[i]$
        \State $S[i] \gets meanShift(S[i], X)$
        \doWhile{$S[i] \neq temp$}
        \EndFor
        \State \Return{$S$}
	\end{algorithmic}
\end{algorithm}

\end{frame}
%----------------------------------------------------------------------------------------`
\begin{frame}
\frametitle{Parallel implementations (2/2)}
We have been implemented two parallel versions using two different types of scheduling provided by OpenMP framework.
\begin{itemize}
\item \textbf{static} scheduling:
\begin{itemize}
\item where the for loop is divided into equal-sized chunks.
\end{itemize}
\item \textbf{dynamic} scheduling:
\begin{itemize}
\item an internal work queue give a chunk-sized block of loop iterations to each thread;
\item when a thread has finished it ask for another block of loop iterations.
\end{itemize}
\end{itemize}
\end{frame}
%----------------------------------------------------------------------------------------
\section{Results}
\begin{frame}
\frametitle{Results (1/2)}
Trends of average \textbf{speedup} in function of number of data points.
\begin{itemize}
\item On a CPU i7-8750H with \textbf{6 cores} (12 threads with hyperthreading).
\item Max average speedup with static scheduling: \textbf{4.04X} on 1K points.
\item Max average speedup with dynamic scheduling: \textbf{7.17X} on 1K points.
\end{itemize}

\begin{center}
\begin{figure}
\end{figure}
\includegraphics[width=\textwidth,height=0.8\textheight,keepaspectratio]{figures/speedups.png}
\end{center}

\end{frame}
%----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Results (2/2)}
\begin{itemize}
\item Average \textbf{speedups} in function of \textbf{number of cores}.
\item With hyperthreading deactivated (i.e. 6 cores and 6 threads).
\item The trend is \textbf{linear}. 
\end{itemize}

\begin{center}
\begin{figure}
\end{figure}
\includegraphics[width=\textwidth,height=0.8\textheight,keepaspectratio]{figures/speedups_vs_ncores.png}
\end{center}

\end{frame}
%----------------------------------------------------------------------------------------
\section{Conclusions}
\begin{frame}
\frametitle{Conclusions}
\begin{itemize}
\item We obtain an average speedup with dynamic scheduling higher than the speedup obtained with static scheduling in all the experiments.
\item This is due to the tradeoff between \textbf{quantity of workload} and the \textbf{awaiting time} until the last thread finish its execution.
\end{itemize}
\end{frame}
%----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Extra - CUDA implementation runtimes comparison}
\begin{itemize}
\item Speedup of \textbf{8.7X} reached with a CUDA kernel implementation on 10K data points.
\end{itemize}

\begin{center}
\begin{figure}
\end{figure}
\includegraphics[width=\textwidth,height=0.8\textheight,keepaspectratio]{figures/speedups_with_CUDA_kernel.png}
\end{center}

\end{frame}
%----------------------------------------------------------------------------------------`
%----------------------------------------------------------------------------------------`
%----------------------------------------------------------------------------------------`
%----------------------------------------------------------------------------------------`
%----------------------------------------------------------------------------------------`

\begingroup
\footnotesize
\begin{frame}
\frametitle{References}

\begin{thebibliography}{99} % Beamer does not support BibTeX so references must be inserted manually as below

\bibitem{cheng1995}{Cheng, Yizong. "Mean shift, mode seeking, and clustering." IEEE transactions on pattern analysis and machine intelligence 17.8 (1995): 790-799.}

\end{thebibliography}

\end{frame}
\endgroup

%------------------------------------------------

\end{document}

