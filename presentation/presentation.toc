\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Kernels}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Mean shift}{5}{0}{1}
\beamer@subsectionintoc {1}{3}{Mean shift procedure}{6}{0}{1}
\beamer@subsectionintoc {1}{4}{Mean Shift Clustering}{7}{0}{1}
\beamer@sectionintoc {2}{Implementations}{8}{0}{2}
\beamer@subsectionintoc {2}{1}{Sequential implementation}{8}{0}{2}
\beamer@subsectionintoc {2}{2}{Parallel implementations}{11}{0}{2}
\beamer@sectionintoc {3}{Results}{13}{0}{3}
\beamer@sectionintoc {4}{Conclusions}{15}{0}{4}
