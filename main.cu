#include "Dataset.h"
#include "MeanShift.h"
#include "Timer.h"
#include <iostream>
#include <cmath>
#include <cuda.h>

#define cudaErrorCheck(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true) {
    if (code != cudaSuccess) {
        fprintf(stderr, "GPU assert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (abort) exit(code);
    }
}

void printMat(float *M, int nRows, int nCols=1) {
    for (int i = 0; i < nRows; ++i) {
        std::cout << i  << " | ";
        for (int j = 0; j < nCols; ++j) {
            std::cout << M[i * nCols + j] << ' ';
        }
        std::cout << '\n';
    }
}

void printMat(int *M, int nRows, int nCols=1) {
    for (int i = 0; i < nRows; ++i) {
        for (int j = 0; j < nCols; ++j) {
            std::cout << M[i * nCols + j] << ' ';
        }
        std::cout << '\n';
    }
}

bool areEquals(const float *lhs, const float *rhs, int nRows, int nCols = 1) {
    float eps = 1e-7;
    for (int i = 0; i < nRows; ++i) {
        for (int j = 0; j < nCols; ++j) {
            if ( std::abs(lhs[i * nCols + j] - rhs[i * nCols + j]) >= 0.005)
                return false;
        }
    }
    return true;
}

__global__ void KernelMeanShift(float *values, int nRows, int nCols,
        float threshold, float bandwidth, float tolerance, int maxIterations,
        float *copy) {
    
    for (int ii = blockIdx.x * blockDim.x + threadIdx.x;
         ii < nRows;
         ii += blockDim.x * gridDim.x) {
        
        float *numerator = new float[nCols];
        float *denominator = new float[nCols];
        float *error = new float[nCols];
        bool isTolerable{true};
        int counter{0};
        
        do {
            // clearing numerator and denominator
            for (int j = 0; j < nCols; ++j) { numerator[j] = 0; }
            for (int j = 0; j < nCols; ++j) { denominator[j] = 0; }

            float dist{0};
            float weight{0};

            // computing the distances from ii obs (copy) and all other obs (values)
            for (int i = 0; i < nRows; ++i) {
                dist = 0;
                weight = 0;
                // computing euclidean distance from ii obs (copy) and i obs (values)
                for (int j = 0; j < nCols; ++j) {
                    dist += (copy[ii * nCols + j] - values[i * nCols + j]) *
                            (copy[ii * nCols + j] - values[i * nCols + j]);
                }
                if (dist < threshold) {
                    weight += std::exp(-0.5 * ( dist / (bandwidth * bandwidth) ));
                    for (int j = 0; j < nCols; ++j) {
                        numerator[j] += weight * values[i * nCols + j];
                        denominator[j] += weight;
                    }
                }
            }

            isTolerable = true;
            for (int j = 0; j < nCols; ++j) {
                error[j] = copy[ii * nCols + j] - numerator[j] / denominator[j];
                error[j] = error[j] > 0 ? error[j] : -error[j];
                if (error[j] > tolerance) {
                    isTolerable = false;
                }
                copy[ii * nCols + j] = copy[ii * nCols + j] - error[j];
                copy[ii * nCols + j] = numerator[j] / denominator[j];
            }

            ++counter;
        } while (!isTolerable && counter < maxIterations);

        delete[] error;
        delete[] denominator;
        delete[] numerator;
    }
}

float *makeCopy(const float *values, const int nRows, const int nCols) {
    auto *valuesCopy = new float[nRows * nCols];
    for (int i = 0; i < nRows; ++i) {
        for (int j = 0; j < nCols; ++j) {
            valuesCopy[i * nCols + j] = values[i * nCols + j];
        }
    }
    return valuesCopy;
}

__host__ float *StubMeanShift(float *values, int nRows, int nCols,
    float threshold, float bandwidth, float tolerance, int maxIterations) {
    // Takes an nxp matrix and return a mean shifted nxp matrix
    
    int memSize = sizeof(float) * nRows * nCols;
    float *d_values;
    cudaMalloc((void **) &d_values, memSize);
    cudaMemcpy(d_values, values, memSize, cudaMemcpyHostToDevice);

    float *copy = makeCopy(values, nRows, nCols);
    float *d_copy;
    cudaMalloc((void **) &d_copy, memSize);
    cudaMemcpy(d_copy, copy, memSize, cudaMemcpyHostToDevice);

    int numSMs;
    cudaDeviceGetAttribute(&numSMs, cudaDevAttrMultiProcessorCount, 0);
    
    KernelMeanShift<<<32 * numSMs, 256>>>(d_values, nRows, nCols,
        threshold, bandwidth, tolerance, maxIterations, d_copy);
    
    float *result = new float[nRows * nCols];
    cudaMemcpy(result, d_copy, memSize, cudaMemcpyDeviceToHost);

    cudaFree(d_copy);
    cudaFree(d_values);

    delete[] copy;

    return result;
}

int main() {
    //std::string filename = "../data/data_100.csv";
    //std::string filename = "../data/data_1000.csv";
    //std::string filename = "../data/data_3000.csv";
    std::string filename = "../data/data_10000.csv";

    constexpr int nRows{10000};
    constexpr int nCols{3};

    std::cout << nRows << '\n';
    constexpr float threshold{1e+7};   
    constexpr float bandwidth{2};
    constexpr float tolerance{1e-7};
    constexpr int maxIterations{100};
    constexpr float eps{0.05};

    Dataset *dataset = new Dataset(filename.c_str(), nRows, nCols);

    Timer t;
    t.start();
    float *shifted_data_seq = MeanShift(dataset->getData(), nRows, nCols, threshold, bandwidth, tolerance, maxIterations);
    float *clusters_seq = makeClusters(shifted_data_seq, nRows, nCols, eps);
    t.stop();
    double timeSeq = t.getElapsedTimeInMilliSec();
    std::cout << "Elapsed time:\t" << timeSeq << " [ms].\n";
    
    t.start();
    float *shifted_data_par = StubMeanShift(dataset->getData(), nRows, nCols, threshold,
        bandwidth, tolerance, maxIterations);
    float *clusters_par = makeClusters(shifted_data_par, nRows, nCols, eps);
    t.stop();
    cudaDeviceSynchronize();
    double timePar = t.getElapsedTimeInMilliSec();
    std::cout << "Elapsed time:\t" << timePar << " [ms].\n";

    std::cout << "SpeedUp:\t" << timeSeq / timePar << " X\n";

    areEquals(clusters_seq, clusters_par, nRows) ? std::cout << "OK.\n" : std::cout << "Not OK.\n";

    auto *clusters_dataset = new Dataset(clusters_par, nRows, 1);
    clusters_dataset->writeToCSV( (filename.substr(0, filename.size() - 4) + "_clusters.csv").c_str() );
    return 0;
}
