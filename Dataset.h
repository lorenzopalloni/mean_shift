//
// Created by lore on 4/13/19.
//

#ifndef MEAN_SHIFT_DATASET_H
#define MEAN_SHIFT_DATASET_H

// #define NDEBUG  // Uncomment to disable assert()

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <cassert>

class Dataset {
private:
    float *m_values{nullptr};
    std::vector<std::string> *m_colNames{nullptr};
    int m_nRows{0};
    int m_nCols{0};

public:
    Dataset(const char *filename, int nRows, int nCols, bool header=false, char sep=',');
    Dataset(float *values, int nRows, int nCols);
    Dataset(float *values, int nRows, int nCols, std::vector<std::string> *colNames);
    ~Dataset() { delete [] m_values; delete m_colNames; }

    float* getData() { return m_values; }
    std::vector<std::string> *getColNames() { return m_colNames; }
    int getNRows() { return m_nRows; }
    int getNCols() { return m_nCols; }

    void setValues(float *values) { m_values = values;}
    void setColNames(std::vector<std::string> *colNames) { m_colNames = colNames; }
    void setNRows(int nRows) { m_nRows = nRows; }
    void setNCols(int nCols) { m_nCols = nCols; }

    void print();
    void head(int n=5);
    void tail(int n=5);
    void writeToCSV(const char *filename, bool header=false, char sep=',');
    Dataset *subset(int n=5);

    static bool isPunctuation(char ch);
    static std::vector<std::string>* splitColNames(std::string line, int nCols, char sep = ',');
    friend std::ostream& operator<<(std::ostream &out, Dataset *dataset);

};

#endif //MEAN_SHIFT_DATASET_H
